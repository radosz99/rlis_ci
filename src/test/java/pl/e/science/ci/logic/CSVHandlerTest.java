package pl.e.science.ci.logic;

import org.junit.Assert;
import org.junit.Test;
import pl.escience.ci.logic.CSVHandler;

import java.util.ArrayList;

public class CSVHandlerTest {
    @Test
    public void getItemsTest() {
        CSVHandler csvHandler = new CSVHandler();
        ArrayList<ArrayList<String>> resultTest = new ArrayList<>();
        ArrayList<String> firstRow = new ArrayList<>();
        firstRow.add("FirstItem");
        firstRow.add("SecondTime");
        resultTest.add(firstRow);
        Assert.assertNull(CSVHandler.getByRow(3,resultTest));
        Assert.assertNull(CSVHandler.getByColumn(2,resultTest));
        Assert.assertNotNull(CSVHandler.getByRow(0,resultTest));
        Assert.assertNotNull(CSVHandler.getByColumn(0,resultTest));
    }

}
